# Service Usage

* HZDR GPU compute

## Plotting

* Plotting is be performed in the [Plotting project](https://gitlab.hzdr.de/hifis/overall/kpi/kpi-plots-ci).

## Data

* KPI-1 e.g. Number of daily internal users - Some explanation e.g. This is the number of unique daily users from providing centre.
* KPI-2 e.g. Number of daily external users - Some explanation e.g. This is the number of unique daily users from external sites.
* KPI-3 e.g. Total used storage - The total used storage in GB.
* KPI-x ....

## Schedule

* daily, weekly
